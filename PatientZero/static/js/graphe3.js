queue()
	.defer(d3.json, "/twitterdb/twitter_search3")
	.await(makeGraphs)

function makeGraphs(error, projectsJson) {

		//if (error) throw error;
	
		var twitterdb = projectsJson;
		var dateFormat = d3.time.format('%a %b %d %H:%M:%S +0000 %Y');
		var dateFormat2 = d3.time.format('%a %b %H:%M:%S %Y');
		var total_tweet = 0;
		//var secondsFORM = d3.time.format('%S');
		twitterdb.forEach(function(d) {
			d["created_at"] = dateFormat.parse(d["created_at"]);
			d["created_at"].setDate(1);
			total_tweet = total_tweet+1; // compte le nombre de tweets totaux présent dans la session
		});
	
	
		// transmission des données à la fonction de filtre croisé
		var ndx = crossfilter(twitterdb);
		var all = ndx.groupAll();
	
			//secondsFORM(d["created_at"])
		// création des dimmentions à examiner
		var dateDim = ndx.dimension(function(d) { return d["created_at"]; });
	
	
		//calcule sur la date
		var nbTweetsPerDate = dateDim.group(); // pour donner ne nb de tweets par moment exacte
	
	
		//Definition des valeurs à utiliser
		var minDate = dateDim.bottom(1)[0]["created_at"];
		var maxDate = dateDim.top(1)[0]["created_at"];
	
	/******************************************Graphiques******************************************************/
	
		var returnsRowChart = dc.barChart("#rowChart");
	
		var totalTweets = dc.numberDisplay("#total");
	
	
		totalTweets
			.formatNumber(d3.format("d"))
			.valueAccessor(function(d){ return d; })
			.group(all)
			.formatNumber(d3.format(".3s"));
	
	
		returnsRowChart
			.width(1400)
			.height(220)
			.margins({top: 10, right: 50, bottom: 30, left: 50})
			.dimension(dateDim)
			.group(nbTweetsPerDate)
			.transitionDuration(5000)
			.x(d3.time.scale().domain([minDate, maxDate]))
			.elasticY(true)
			//.elasticX(true)
			.xAxisLabel('Dates')
			.yAxisLabel('Nombre de Tweets')
			.xUnits(function(){return d3.timeDays;})
			//.xUnits(function(){return 17;})
			.centerBar(true)
			.gap(30)
			.xAxis().tickFormat(dateFormat2);
			//.xAxisPadding()
			//.yAxis().ticks(4)
	
	
		dc.renderAll();
	
};
