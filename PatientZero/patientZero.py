from __future__ import print_function
from flask import Flask, render_template, url_for, request, redirect
from form import TwitterForm
from pymongo import MongoClient
import tweepy
import json
from bson import json_util
from bson.json_util import dumps
from random import *

app = Flask(__name__) #instantiation de l'objet app
#app.secret_key = '2d9-E2.)f&é,A$p@fpa+zSU03êû9_'

MONGO_HOST= 'mongodb://localhost/twitterdb' 

CONSUMER_KEY = "ZMjopMwhpZmCLoCg99bY35OHQ"
CONSUMER_SECRET = "fVouFDTmajUb5VeyYrzvFgxjoAcXral8r1mn4Hxt1vosWhZluM"
ACCESS_TOKEN = "1090937742601699328-WGyBvilXfqq7vf0pBNpaj0gf7FEBLy"
ACCESS_TOKEN_SECRET = "C0cjXOwpZXquP76irIN6EAIlV2HB2ywJUNeMBWD9pdeOB"

COLLECTION_NAME = 'sick'
COLLECTION_NAME2 = 'malade'

MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
DBS_NAME = 'twitterdb'
FIELDS = {'created_at': True, 'text': True, 'geo': True, 'id': True, 'coordinates': True, 'place': True}


#####################CLASSE POUR RECUPERER LES TWEETS##############################################################################################

class StreamListener(tweepy.StreamListener):    

    def on_connect(self):
        # Appelé innitialement pour se connecter à l'API
        print("Vous êtes maintenant connecté à l'API de stream")
        
    def on_error(self, status_code):
        # On error - en cas d'erreur, affiche l'erreur et le code de l'erreur
        print('An Error has occured: ' + repr(status_code))
        return False

    def on_data(self, data):
    #Element principal de la classe... on se connexte à mongoDB et stock les tweets
        try:
            client = MongoClient(MONGO_HOST)
            
            
            # Utilise la base de données twitterdb. 
            db = client.twitterdb
            
            # Decode le JSON de Twitter
            datajson = json.loads(data)
    
            # Recupere le champs 'created_at' du Tweet pour l'affichage
            created_at = datajson['created_at']
            # Recupere le champs 'geo' du Tweet pour l'affichage
            where= datajson['geo']
            

            # affiche sur le terminal qu'on a collecté un Tweet
            print("Tweet collected at " + str(created_at) + " in " + str(where))
            print(COLLECTION)
            #insert la donnée dans mongoDB dans la collection appelé COLLECTION
            
            db[COLLECTION].insert(datajson)
            
            return datajson
            
        except Exception as e:
            print(e)


###########################################################################################################################################

@app.route('/') #correspond à une vue
def index():
    client = MongoClient(MONGO_HOST).twitterdb
    colls = MongoClient(MONGO_HOST).twitterdb.collection_names()
    #call = MongoClient(MONGO_HOST).twitterdb.command("dbstats")
    #datasize = call['dataSize'] / 1024
    #print(datasize)
    return render_template('best.html', cols=colls, cli=client)

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
listener = StreamListener(api=tweepy.API(wait_on_rate_limit=True)) 
streamer = tweepy.Stream(auth=auth, listener=listener)

@app.route('/Research', methods=['GET', 'POST'])
def Research():
    if request.method == 'POST':
        global COLLECTION 
        COLLECTION = '{msg}'.format(msg=request.form['msg'])
        print("Session:" + str(COLLECTION))
        global MOT
        mots_cles = '{mots}'.format(mots=request.form['mots'])
        MOT = '{mots}'.format(mots=request.form['mots'])
        print("Mot clé:" + str(MOT))
        WORDS = ['{msg}'.format(msg=request.form['msg']) + " " + '{mots}'.format(mots=request.form['mots'])]
        
        if request.form['boutton'] == 'Envoyer':
            print("Tracking: " + str(WORDS))
            streamer.filter(track=WORDS, is_async=True)
            i = 0
            while i!=1:
                if request.form['boutton'] == 'stop':
                    streamer.disconnect()
                    i = 1
        streamer.disconnect()
        print("collecte terminée !")
        return redirect(url_for("visu"))
        
    return render_template('Twitter2.html', title='recherche')



@app.route('/visu')
def visu():
    #COLLECTION = Collection
    return render_template("visu.html")

@app.route('/visua')
def visua():
    #COLLECTION = Collection
    return render_template("visua.html")

@app.route('/visub')
def visub():
    #COLLECTION = Collection
    return render_template("visub.html")

################## TEST D'UNE COLLECTION EN PARTICULIER ##################################################################
@app.route("/twitterdb/twitter_search2")
def twitterdb_twitter_search2():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DBS_NAME][COLLECTION_NAME]
    projects = collection.find(projection=FIELDS, limit=1000)
    print(projects)
    """ ici on change les coordonnées de la collection
    client = MongoClient(MONGO_HOST)
    db = client.twitterdb
    res = db[COLLECTION_NAME].find().limit(5)
    #print("debut")
    print(COLLECTION_NAME)
    for doc in res :
        #print("*",doc)
        x = uniform(-180,180)
        y = uniform(-85,85)
        doc['geo'] = {'type': 'Point', 'coordinates': [x, y]}
        doc['coordinates'] = [y, x]
        db[COLLECTION_NAME].save(doc)
    #print('tot')
    """
    json_projects = []
    for project in projects:
        json_projects.append(project)
    print(len(json_projects))    
    json_projects = json.dumps(json_projects, default=json_util.default)
    print(len(json_projects))
    #for e in range (len(json_projects)):
    #    print(json_projects[e])
    connection.close()
    return json_projects

##################TESTE D'UNE COLLECTION EN GENERALE##################################################################
@app.route("/twitterdb/twitter_search")
def twitterdb_twitter_search():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DBS_NAME][COLLECTION]
    projects = collection.find(projection=FIELDS, limit=1000)
    print(projects)
    """ ici on change les coordonnées de la collection
    client = MongoClient(MONGO_HOST)
    db = client.twitterdb
    res = db[COLLECTION].find().limit(20)
    #print("debut")
    print(COLLECTION)
    for doc in res :
        #print("*",doc)
        x = uniform(-180,180)
        y = uniform(-85,85)
        doc['geo'] = {'type': 'Point', 'coordinates': [x, y]}
        doc['coordinates'] = [y, x]
        db[COLLECTION].save(doc)
    #print('tot')
    """
    json_projects = []
    for project in projects:
        json_projects.append(project)
    print(len(json_projects))    
    json_projects = json.dumps(json_projects, default=json_util.default)
    print(len(json_projects))
    connection.close()
    return json_projects

###############################################################################

@app.route("/twitterdb/twitter_search3")
def twitterdb_twitter_search3():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DBS_NAME][COLLECTION_NAME2]
    projects = collection.find(projection=FIELDS, limit=1000)
    print(projects)
    """ ici on change les coordonnées de la collection"""
    client = MongoClient(MONGO_HOST)
    db = client.twitterdb
    res = db[COLLECTION_NAME].find().limit(20)
    #print("debut")
    print(COLLECTION_NAME)
    for doc in res :
        #print("*",doc)
        x = uniform(-180,180)
        y = uniform(-85,85)
        doc['geo'] = {'type': 'Point', 'coordinates': [x, y]}
        doc['coordinates'] = [y, x]
        db[COLLECTION_NAME].save(doc)
    #print('tot')
    
    json_projects = []
    for project in projects:
        json_projects.append(project)
    print(len(json_projects))    
    json_projects = json.dumps(json_projects, default=json_util.default)
    print(len(json_projects))
    #for e in range (len(json_projects)):
    #    print(json_projects[e])
    connection.close()
    return json_projects


if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5620,debug=True)




   





