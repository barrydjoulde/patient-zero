from __future__ import print_function
import tweepy
import json
import time
from pymongo import MongoClient


MONGO_HOST= 'mongodb://localhost/twitterdb'  
# assuming you have mongoDB installed locally
# and a database called 'twitterdb'

WORDS = '#FRAISL'
CONSUMER_KEY = "6IcIWVIQ2SMLlAzjyWWjBhrYW"
CONSUMER_SECRET = "SvrFd160mS4CA42CImmUpJqvNEIOzDdCWpAImrdaHlx7MkGW7T"
ACCESS_TOKEN = "1090937742601699328-Sm9CX7TSOCYCkqsW9g6KQOr4e5UF1t"
ACCESS_TOKEN_SECRET = "ugTEVvYCoR6DzFoQQwVZ4v8e0nE9WZ0VuCsklKjohbGNz"


class StreamListener(tweepy.StreamListener):    
    
    #This is a class provided by tweepy to access the Twitter Streaming API. 

    def on_connect(self):
        # Called initially to connect to the Streaming API
        print("You are now connected to the streaming API.")
        
    def on_error(self, status_code):
        # On error - if an error occurs, display the error / status code
        print('An Error has occured: ' + repr(status_code))
        return False

    def on_data(self, data):
    #This is the meat of the script...it connects to your mongoDB and stores the tweet
        try:
            client = MongoClient(MONGO_HOST)
            
            # Use twitterdb database. If it doesn't exist, it will be created.
            db = client.twitterdb
            
            # Decode the JSON from Twitter
            datajson = json.loads(data)
    
            #grab the 'created_at' data from the Tweet to use for display
            created_at = datajson['created_at']
            ts = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(datajson['created_at'],'%a %b %d %H:%M:%S +0000 %Y'))
            #grab the 'geo' data from the Tweet to use for display
            where= datajson['geo']

            #print out a message to the screen that we have collected a tweet
            print("Tweet collected at " + str(ts) + " in " + str(where))
            #insert the data into the mongoDB into a collection called twitter_search
            #if twitter_search doesn't exist, it will be created.
            db.twitter_search.insert(datajson)
            return datajson
        except Exception as e:
            print(e)



auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    #Set up the listener. The 'wait_on_rate_limit=True' is needed to help with Twitter API rate limiting.
listener = StreamListener(api=tweepy.API(wait_on_rate_limit=True)) 
streamer = tweepy.Stream(auth=auth, listener=listener)
print("Tracking: " + str(WORDS))
streamer.filter(track=WORDS)



