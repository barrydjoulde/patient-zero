from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import data_required,length

class TwitterForm(FlaskForm):
    
    subject = StringField('Mot(s) clé(s)', validators=[ data_required(), length(min=2, max=20)])

    submit= SubmitField('Chercher')