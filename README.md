# Patient Zéro

# A propos du projet

Nous réalisons ce projet en tant que travail d'étude et de recherche de licence informatique.

Patient Zéro est une plateforme réalisé avec le langage python (flask), MongoDb, javascript, html, Css qui a pour but d'utiliser l'API Twitter pour analyser des données afin de mettre en évidence des débuts de propagation d'épidémies.
